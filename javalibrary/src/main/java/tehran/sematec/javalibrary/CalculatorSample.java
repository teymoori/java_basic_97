package tehran.sematec.javalibrary;

import java.util.Scanner;

public class CalculatorSample {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        log("enter first number");
        int fNumber = sc.nextInt();

        log("enter second number");
        int sNumber = sc.nextInt();

        log("enter action");
        String action = sc.next();

        float result = 0;

        switch (action) {
            case "+":
                result = fNumber + sNumber;
                break;
            case "-":
                result = fNumber - sNumber;
                break;
            case "*":
                result = fNumber * sNumber;
                break;
            case "/":
                try{
                    result = fNumber / sNumber;

                }catch (Exception e){
                    result = fNumber ;
                }

                break;
            default:
                log("action not valid");
        }

        if (result != 0) {
            log("result is:" + result);
        }


    }

    public int getTangant(){
        return 80 ;
    }

    int sin(int number){
        return 90 ;
    }

    static void log(String msg) {
        System.out.println(msg);
    }
}
