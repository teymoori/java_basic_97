package tehran.sematec.javalibrary;

import java.util.Scanner;

public class ScannerSample {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        log("pls enter ur name :");

        String name = sc.next();

        log("you entered " + name);

        log("enter your age:");

        int age = sc.nextInt();

        log("your age is " + age);



    }

    static void log(String msg){
        System.out.println(msg);
    }
}
