package tehran.sematec.javalibrary;

public class Statements {

    public static void main(String[] args) {
        int age = 30;
        if (age != 30) {
            System.out.println("Your are 30");
            System.out.println("You are from Iran");
        } else if (age == 20)
            System.out.println("Your are young");
        else
            System.out.println("i don`t know");

        boolean isAlive = true;
        if (isAlive) {
            System.out.println("You are alive");
        } else
            System.out.println("you death");


        String myName = "Amir";
        String firstName = "amir";

        if (myName.equals(firstName)) {
            System.out.println("myName equals firstName");
        } else
            System.out.println("they are not");

        if (myName.equalsIgnoreCase(firstName)) {
            System.out.println("equalsIgnoreCase");
        }

        if (myName.toLowerCase().equals(firstName.toLowerCase())) {

        }

        if (myName.toUpperCase().equals(firstName.toUpperCase())) {

        }

        String englishNumbers =  "1234567890";
        String persianNumbers =  "۱۲۳۴۵۶۷۸۹۰";


        String username = "admini623tr84" ;

        if( username.startsWith("admin")){
            //true
        }else
            //not true


        System.out.println( clean("my age is ۳۰"));


        int price = 30000 ;

        if( price>=2000){
            //print expensive
        }else{
            //not
        }

        String strPrice = price+"" ;
        String strPriceB = String.valueOf(price) ;

        String strAe = "30" ;

        //Integer.parseInt(strAe)

        System.out.print("price is " + price);



        int myAge = 40 ;
        String name = "AmirHossein" ;

        if( myAge==40 && name.equals("mohammad")){
            //false
        }

        if( myAge==40 || name.equals("ali")){
            //true
        }

        if(
                (myAge==20 && name.equals("ali"))
                || (myAge==40)){

        }

        if( myAge==20 && name.equals("ali")){

        }



    }
    static String clean(String originalWord){
        return originalWord
                .replace("۰","0")
                .replace("۱","1")
                .replace("۲","2")
                .replace("۳","3")
                .replace("۴","4")
                .replace("۵","5")
                .replace("۶","6")
                .replace("۷","7")
                .replace("۸","8")
                .replace("۹","9") ;
    }


}
